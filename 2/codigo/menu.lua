local notas = {}

local opcoes = {
    c = function()
        local nome = io.read()
        local nota = notas[nome]
        
        if nota then
            io.write(nota, "\n")
        else
            io.write(nome, " n�o possui nota associada.\n")
        end
    end,

    a = function()
        local nome, nota = io.read(), tonumber(io.read())
        if nota and nota <= 10 and nota >= 0 then -- nota ~= nil e dentro do intervalo -> formato v�lido
            notas[nome] = nota
            io.write("Nota atualizada.\n")
        else
            io.write("Nota inv�lida.\n")
        end
    end
}

local op
repeat
    op = string.lower(io.read())
    local f = opcoes[op]
    if f then
        f()
    end
until op == "f"