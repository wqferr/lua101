function foreach(array, f)
	for i = 1, #array do
		f(array[i])
	end
end

local a = {"x", "y", "z"}
foreach(a, print)
print("=====")
foreach(a, io.write)