function foreach(array, f)
	for i = 1, #array do
		f(array[i])
	end
end

local a = {
	function()
		io.write("oi\n")
	end,
	function()
		io.write("oi de novo\n")
	end,
	function()
		io.write("tchau\n")
	end
}

--[[
for i = 1, #a do
	local f = a[i]
	f() -- a[i]()
end
]]

foreach(
	a,
	function(f)
		f()
	end
)
