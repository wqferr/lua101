function buscaBinaria(array, v, inicio, fim)
	inicio = inicio or 1
	fim = fim or #array
	if inicio > fim then
		return 0
	end
	
	local meio = (inicio + fim) // 2
	if array[meio] == v then
		return meio
	elseif array[meio] < v then
		return buscaBinaria(array, v, meio + 1, fim)
	else
		return buscaBinaria(array, v, inicio, meio - 1)
	end
end