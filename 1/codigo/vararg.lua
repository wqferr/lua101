function max(...)
	local vals = {...}
	local m = vals[1]
	
	for i = 2, #vals do
		if vals[i] > m then
			m = vals[i]
		end
	end
	
	return m
end

function lim(...)
	local vals = {...}
	local m = vals[1]
	local M = m
	
	for i = 2, #vals do
		if vals[i] < m then
			m = vals[i]
		end
		if vals[i] > M then
			M = vals[i]
		end
	end
	
	return m, M
end

function maxR(v1, ...)
	local resto = {...}
	if #resto == 0 then
		return v1
	end
	local maxResto = maxR(...)
	if maxResto > v1 then
		return maxResto
	else
		return v1
	end
end

function soma(v1, ...)
	local resto = {...}
	if #resto == 0 then
		return v1
	end
	return v1 + soma(...)
end

function inverte(v1, ...)
	local resto = {...}
	if #resto == 0 then
		return v1
	end
	local result = {inverte(...)}
	result[#result+1] = v1
	return unpack(result)
end