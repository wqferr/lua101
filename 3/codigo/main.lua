local Complex = require("complex")

local a = Complex.new(1, 2)
local b = Complex.new(-1, -1)

local f = function(opName, ...)
	print(Complex.toString(Complex[opName](...)))
end

f("add", a, b)
f("sub", a, b)
f("mul", a, b)
f("div", a, b)
f("conjugate", a)
f("conjugate", b)
f("reciprocal", a)
f("reciprocal", b)

print(Complex.abs(a))
print(Complex.abs(b))