local Complex = {}

Complex.new = function(r, i)
	local c = {}
	
	c.r = r
	c.i = i
	
	return c
end

Complex.add = function(c1, c2)
	return Complex.new(c1.r + c2.r, c1.i + c2.i)
end

Complex.sub = function(c1, c2)
	return Complex.new(c1.r - c2.r, c1.i - c2.i)
end

Complex.mul = function(c1, c2)
	return Complex.new(c1.r*c2.r - c1.i*c2.i, c1.r*c2.i + c1.i*c2.r)
end

Complex.div = function(c1, c2)
	return Complex.mul(c1, Complex.reciprocal(c2))
end

Complex.reciprocal = function(c)
	local conj = Complex.conjugate(c)
	local d = c.r*c.r + c.i*c.i
	conj.r = conj.r / d
	conj.i = conj.i / d
	
	return conj
end

Complex.conjugate = function(c)
	return Complex.new(c.r, -c.i)
end

Complex.abs = function(c)
	return math.sqrt(c.r*c.r + c.i*c.i)
end

Complex.toString = function(c)
	return string.format("%.2f + %.2fi", c.r, c.i)
end

return Complex
