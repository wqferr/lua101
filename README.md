# LEIAME #

O progresso das aulas se encontra no arquivo `aulas.zip` na seção `Downloads` à esquerda.

# Data da próxima aula: 13 de Junho #
## Aulas dadas: 3 ##
## Próximo assunto: Tabelas como registros ##
:moon:
### O que é isto? ###

Este repositório guarda o progresso das aulas de Lua no campus de São Carlos da USP.
Serão salvos tanto os códigos produzidos quanto os slides utilizados em cada uma das aulas.

### Onde são essas aulas? Posso participar? ###

As aulas se dão em salas aleatórias do ICMC. Mais informações serão
dadas quando as aulas realmente começarem. As aulas serão abertas a todos os interessados.

### Como serão dadas as aulas? ###

As aulas terão início com slides sobre o tópico em mão, seguidos de exemplos e prática de codificação.

### Sou um aluno exemplar e quero estudar o conteúdo da próxima aula com antecedência, como proceder? ###

Para sua sorte, o material da aula seguinte estará disponível no branch `proxima_aula` assim que estiver apresentável. Os assuntos abordados estarão presentes no arquivo `assuntos.txt`. É necessário baixar o repositório inteiro e trocar o `branch` atual para `proxima_aula` para que os arquivos apareçam.

### Referências ###

* [Documentação oficial](https://www.lua.org/docs.html)
    - [Manual de referência em Inglês (5.3)](https://www.lua.org/manual/5.3/)
    - [Manual de referência em Português (5.2)](https://www.lua.org/manual/5.2/pt/)