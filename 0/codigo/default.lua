local str = io.read()

-- tonumber retorna nil se str não for conversível a um número
-- Se for o caso, a expressão avalia em 0, devido ao funcionamento
-- do operador or.
-- Caso contrário, n tem o valor entrado pelo usuário convertido
-- a um número.
local n = tonumber(str) or 0

print(n, type(n))
