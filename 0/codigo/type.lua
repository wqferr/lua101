﻿
-- declaração de variável + inicialização
-- idem a
-- local x
-- x = 42
local x = 42

-- imprime uma string com o tipo de x
print(type(x))

-- reatribuição de x
x = "abc"
print(type(x))

-- type(type(x)) -> type("number") -> "string"
print(type(type(x)))
