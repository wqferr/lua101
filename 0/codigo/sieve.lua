local n = 200

-- Só faz sentido checar até a raiz quadrada do número.
-- (Por quê?)
local lim = math.floor(math.sqrt(n))

-- Array booleano
-- prime[i] == true <=> i é primo
local prime = {}

-- 1 NÃO É PRIMO
prime[1] = false

-- Assumir todos os números como primos inicialmente.
-- Setar como compostos todos os encontrados
-- no laço mais interno.
for i = 2, n do
    prime[i] = true
end

for i = 2, lim do
    if prime[i] then
        -- i é primo, pois nenhum dos números anteriores
        -- é fator dele
        for j = i*i, n, i do
            -- i*i é o primeiro múltiplo de i que ainda não foi marcado.
            -- Incrementos em i para marcar somente múltiplos de i.
            prime[j] = false
        end
    end
end

for i = 1, n do
    if prime[i] then
        print(tostring(i).." é primo")
    end
end
