﻿local array = {1, 5, 4, 3, 3, 2, 4}

for i = #array, 2, -1 do -- posição a ser consertada
    for j = 1, i-1 do -- índice de trocas atual
        if array[j] > array[j+1] then -- deve trocar com a próxima posição
            local aux = array[j]
            array[j] = array[j+1]
            array[j+1] = aux
        end
    end
end

print() -- pular mais uma linha
print("============")
for i = 1, #array do
    print(array[i])
end
print("============")
